# Changelog for "project name"

## [Unreleased]
- description (link to related issue on redmine)
- description (link to related issue on redmine)
- ...

## [v1.1.0] [r4.19.0] - 2020-01-31
### New Features
- description (link to related issue on redmine)
- description (link to related issue on redmine)
- description (link to related issue on redmine)
- ...

### Enhancements
- description (link to related issue on redmine)
- ...
- ...

### Fixes
- description (link to related issue on redmine)
- ...
- ...

## [v1.0.0] [r4.15.0] - 2019-10-20
### New Features
- description (link to related issue on redmine)
- description (link to related issue on redmine)
- description (link to related issue on redmine)
- ...

### Fixes
- description (link to related issue on redmine)
- ...
- ...

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
